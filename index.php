<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Log In | VenBlaCar</title>
    <link rel="shortcut icon" href="dist/img/fevicon.png">
  
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  
    <!-- Bootstrap 3.3.7 -->
    <meta http-equiv="cache-control" content="no-cache">
    <link rel="stylesheet" href="SuperAdmin/bower_components/bootstrap/dist/css/bootstrap.min.css">
  
    <!-- Font Awesome -->
    <link rel="stylesheet" href="SuperAdmin/bower_components/font-awesome/css/font-awesome.min.css">
    
    <!-- Ionicons -->
    <link rel="stylesheet" href="SuperAdmin/bower_components/Ionicons/css/ionicons.min.css">
    
    <!-- Theme style -->
    <link rel="stylesheet" href="dist/css/AdminLTE.min.css">
    
    <!-- Theme style -->
    <link rel="stylesheet" href="dist/css/login_style.css">
    
    <!-- iCheck -->
    <link rel="stylesheet" href="plugins/iCheck/square/blue.css">

    <!-- Google Font -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
    
<style>
.nav>li>a {
    position: relative;
    display: block;
    padding: 10px 22px;
    font-size: 16px;
}
.login-box-body, .register-box-body {
    background: #fff;
    padding: 18px;
    border-top: 0;
    color: #666;
}
.nav-tabs-custom {
    margin-bottom: 20px;
    background: #fff;
    box-shadow: 0 1px 1px rgba(0,0,0,0.1);
    border-radius: 3px;
    position: relative;
    top: -14px;
} 

@media screen and (min-width: 768px) and (max-width: 1024px){
.nav>li>a {
    position: relative;
    display: block;
    padding: 10px 32px;
    font-size: 14px;
} }

@media screen and (min-width: 320px) and (max-width: 667px){    
.nav>li>a {
    position: relative;
    display: block;
    padding: 9px 9px;
    font-size: 12px;
} }
</style>
</head>

<body class="hold-transition login-page">
<br><br><br>

<!-- Login Start -->
    <div class="login-box">
        <div class="login-logo">
            <a href="index.php"><img src="dist/img/logo_trans.png"></a>
        </div>
    
        <div class="login-box-body" style="border-radius: 10px;">
            <div class="row">
                                        
                <!-- Custom Tabs -->
                <div class="nav-tabs-custom">
                    <div class="tab-content">
                        <div class="tab-pane active" id="tab_1">
                      
                            <!--- Begin Login --->
                            <?php
                                 include('config.php');
                                 if(isset($_POST['admin']) && !empty($_POST['admin']))
                                 {
                                    $name=$_POST['name']; 
                                    $pass=$_POST['password'];
           
                                    $sql=mysqli_query($con,"SELECT * FROM admin WHERE email='$name'");
                                    $counnt=mysqli_num_rows($sql);
                                    if($counnt>0)
                                    {
                                       $rows=mysqli_fetch_assoc($sql);
                                       $uname=$rows['email'];
                                       $password=$rows['password'];
                                       $user_type=$rows['user_type'];
                                       $status=$rows['status'];
                   
                                       if($user_type=='Administrator' && $status=='Active')
                                       {
    		                              if($name==$uname && $pass==$password)
    		                              {
    		                                 $_SESSION['id'] =$rows['id'];
    		                                 echo'<script>window.location="SuperAdmin/dashboard.php";</script>';
    		                              }  
                                          else
                                          {
                                              $perr="Invalid Login";
                                              echo "<script type='text/javascript'>alert(\"$perr\");</script>";
                                          }
                                       }
                                       else
                                       {
                                          $n="Admin not activate!..";
                                          echo "<script type='text/javascript'>alert(\"$n\");</script>"; 
                                       }
                                     }
                                     else
                                     {
                                        $n="No record found";
                                        echo "<script type='text/javascript'>alert(\"$n\");</script>";
                                     }
                                  }
                            ?>
                            <!--- Eng Login --->
                            
                            <?php/* include('new_translate.php');*/?>
                            <div class="login_box_head"><h4>Administrator</h4></div>
                                <form  method="post" enctype="multipart/form-data">
                                     <div class="form-group has-feedback">
                                         <label for="email">Administrator E-mail</label>
                                         <input type="text" class="form-control" placeholder="username" name="name" required>
                                         <span class="glyphicon glyphicon-envelope form-control-feedback" style="background: #00424E!important;border: 1px solid #00424E!important;"></span>
                                     </div>
                                     <div class="form-group has-feedback">
                                         <label for="password">Password</label>
                                         <input type="password" class="form-control" placeholder="password" name="password" required>
                                         <span class="glyphicon glyphicon-lock form-control-feedback" style="background: #00424E!important;border:1px solid #00424E!important;"></span>
                                     </div>
                                     <div class="row mt30">
                                         <div class="col-sm-12 text-left">
                                             <input type="submit" class="btn_login" style="border-radius: 5px;font-size:18px;" name="admin" value="Sign In">
                                         </div>
                                     </div>
                                </form>
                            </div>
                        </div>
                        <!-- /.tab-content -->
                    </div>
                    <!-- nav-tabs-custom -->  
                </div>
                <!-- Custom Tabs -->
            </div>
            <!-- /.login-box-body -->
        </div>
        <!-- /.login-box -->
    </div>

    <!-- jQuery 3 -->
    <script src="bower_components/jquery/dist/jquery.min.js"></script>
    
    <!-- Bootstrap 3.3.7 -->
    <script src="bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
    
    <!-- iCheck -->
    <script src="plugins/iCheck/icheck.min.js"></script>
    
    <script>
      $(function () {
        $('input').iCheck({
          checkboxClass: 'icheckbox_square-blue',
          radioClass: 'iradio_square-blue',
          increaseArea: '20%' /* optional */
        });
      });
    </script>
</body>
</html>
